#include <iostream>

using namespace std;

void a()
{
    cout << "         " << endl;
    cout << "         " << endl;
    cout << "   /\\   " << endl;
    cout << "  /  \\ " << endl;
    cout << " / -- \\ " << endl;
    cout << "/      \\" << endl;

}

void n()
{
    cout << "|      |" << endl;
    cout << "|\\    |" << endl;
    cout << "| \\   |" << endl;
    cout << "|  \\  |" << endl;
    cout << "|   \\ |" << endl;
    cout << "|      |" << endl;
}

void d()
{
    cout << "|==\\  " << endl;
    cout << "|   \\ " << endl;
    cout << "|    | " << endl;
    cout << "|    | " << endl;
    cout << "|    | " << endl;
    cout << "|== /  " << endl;
}

void r()
{
    cout << "|====  " << endl;
    cout << "|   |  " << endl;
    cout << "|   |  " << endl;
    cout << "|===   " << endl;
    cout << "|  \\  " << endl;
    cout << "|   \\ " << endl;
}

void e()
{
    cout << "|===== " << endl;
    cout << "|      " << endl;
    cout << "|      " << endl;
    cout << "|===== " << endl;
    cout << "|      " << endl;
    cout << "|===== " << endl;
}

void w()
{
    cout << "|      |" << endl;
    cout << "|      |" << endl;
    cout << "|      |" << endl;
    cout << "|  ||  |" << endl;
    cout << "\\ /\\ /" << endl;
    cout << "  /  \\ " << endl;
}


int main()
{
    a();
    n();
    d();
    r();
    e();
    w();
    cout << "Hello world!" << endl;
    return 0;
}
